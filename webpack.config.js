const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const webpack = require('webpack');

config = {
	entry: ['./assets/js/index.js', './assets/scss/main.scss'],
	output: {
		filename: 'js/bundle.js',
		path: path.resolve(__dirname, 'static')
	},
	module: {
		rules: [
			{
				test: /\.(css|sass|scss)$/,
				use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								minimize: true,
								importLoaders: 2
							}
						},
						{
							loader: 'postcss-loader'
						},
						{
							loader: 'sass-loader'
						}
					]
				}
				),
			}
		]
	},
	plugins: [
		new ExtractTextPlugin({
			filename: 'css/bundle.css',
			allChunks: true
		}),
		require('autoprefixer')
	]
};

module.exports = config;