---
title: "Super Metroid Arcade"
author: ["Lioran", "Tewal"]
games: ["Super Metroid"]
tags: ["route"]
subtitle: "King Of Kraid"
date: 2017-12-10T10:20:00
layout: single
version: "1.02"
links:
  homepage: "http://metroidconstruction.com/hack.php?id=357"
screenshots:
  - image: "/img/randomizers/super-metroid-arcade/sm002-fs8.png"
    alt: "Samus at the door to Wave Beam. 'The Long Way' is listed as an achievement for the progress made in the room"
  - image: "/img/randomizers/super-metroid-arcade/sm005-fs8.png"
    alt: "Samus fighting Mother Brain"
  - image: "/img/randomizers/super-metroid-arcade/sm007.png"
    alt: "The game over screen lets players enter their run scores to the online leaderboard"
---
Super Metroid Arcade is a romhack of Super Metroid which brings a score-based system, and randomizes the rooms that you go through.<!--more-->

Other changes include:

  - Enemy damage increases as you play
  - Gravity Suit, Space Jump, Plasma Beam, and Screw Attack are temporary items that disappear some time after collection
  - Mother Brain is a normal boss, and does not trigger the ending sequence
  - Achievements for performing various activities
  - Difficulty settings, so you can play at your pace
  - Time attack mode, for you speedrunners
  - An online leaderboard, to encourage competition

You will need the ledge grab to progress through some rooms.
