---
title: "Super Metroid Randomizer"
author: ["Dessyreqt"]
games: ["Super Metroid"]
tags: ["items"]
subtitle: "An item randomizer for Super Metroid"
date: 2016-07-15T00:00:00
layout: single
version: "v21"
links:
  code: https://github.com/Dessyreqt/smrandomizer/
  homepage: "https://dessyreqt.github.io/smrandomizer/"
screenshots:
  - image: "super-metroid-randomizer-01.png"
    alt: "Samus 1"
  - image: "super-metroid-randomizer-02.png"
    alt: "Samus 2"
  - image: "super-metroid-randomizer-03.png"
    alt: "Samus 3"
---
This randomizer simply moves items in Super Metroid to different locations. By specifying a seed, this can be used to race other players.<!--more-->

There are a number of options:

  - Old Randomizer---For people who have played old versions of the randomizer and prefer the feel of it
  - Casual
  - Speedrunner
  - Masochist

When playing the Speedrunner or above difficulties, you will need to know techniques in order to progress.

  - Mockball
  - Shortcharge
  - Continuous Wall Jump
  - Gravity Jump
  - Green Gate Glitch
  - Zebetite Glitch
