---
title: "Castlevania: Aria of Sorrow Item Randomizer"
author: ["Abyssonym"]
games: ["Castlevania", "Aria of Sorrow"]
randomizes: ["items"]
subtitle: "Item randomizer for Castlevania: Aria of Sorrow"
date: 2017-06-02T00:00:00
layout: single
version: "8"
links:
  code: https://github.com
  homepage: https://github.io
screenshots:
  - image: "image1.png"
    alt: ""
  - image: "image2.png"
    alt: ""
---
Test<!--more-->