module.exports = {
  parser: 'postcss-scss',
  sourceMap: false,
  plugins: {
  	'autoprefixer': true
  }
};
