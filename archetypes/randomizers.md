---
title: "Randomizer Name"
author: ["Programmers"]
categories: ["Game"]
tags: ["whatisrandomized"]
subtitle: "Subtitle"
date: 2017-12-10T10:20:00
layout: single
version: "version"
links:
  code: https://github.com
  homepage: https://github.io
screenshots:
  - "image1.png"
  - "image2.png"
screenshots-alt:
  - ""
  - ""
---
